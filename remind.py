#!/usr/bin/python3.8

from getopt import getopt
from _glob import *

import dateparser
import json
import time
import sys


if __name__ == "__main__":
  try:
    # parse the option -t
    opts, args = getopt(sys.argv[1:], "t:", [ "time=" ])
    future_time = None

    # the message is whatever remains
    message = " ".join(args)

    for key, val in opts:
      if key in ("-t", "--time"):
        future_time = val
    
    # if the future time is non-existent fuck it
    if not future_time:
      print("Please use the -t or --time option.")
      exit()

    # use the dateparser lib to parse the date string
    future_time = dateparser.parse(future_time)
    # using the walrus operator ;o
    if (future_time := future_time.timestamp()) < time.time():
      print("The future date cannot in the past (for obvious reasons).")
      exit()
    
    # the reminder obj
    reminder_obj = {
      "text": message,
      "time": future_time,
    }

    # write the default file
    # if required
    write_default()

    # open, read, and write the file
    with open(reminders_file, "r+") as file_:
      reminders = json.load(file_)["reminders"]
      
      # add the object
      reminders.append(reminder_obj)
      # rewrite
      write_reminders(file_, reminders)
  except KeyboardInterrupt:
    print("Exiting due to escape key...")
    exit(-1)
