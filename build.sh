#!/bin/bash


BIN="$HOME/bin"

if [ ! -d $BIN ]; then
  echo "Creating $BIN"
  mkdir $BIN
fi

for file in *
do
  file_name=$(basename -- $file)
  ext=${file_name##*.}
  if [ ! $ext == "py" ]; then
    continue
  fi
  echo "Copying $file_name into $BIN"
  cp "$file" "$BIN/${file_name%.*}"
done

echo "Finished"
