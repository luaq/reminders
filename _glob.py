from os import path

import json


title_text     = "Reminder"
reminders_file = "reminders.json"


def write_default(force: bool = False):
  if not force and path.exists(reminders_file):
    return  # only create the file if it needs to be

  # write the file if it doesn't exist  
  with open(reminders_file, "w") as file_:
    json.dump({ "reminders": [] }, file_)


def write_reminders(fp, new_list: list):
  if not fp.writable():
    return
  
  fp.seek(0)
  json.dump({ "reminders": new_list }, fp)
  
  # this is **important**
  fp.truncate()
