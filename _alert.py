#!/usr/bin/python3.8

from _glob import *
from os import path

import subprocess
import os, sys
import atexit
import json
import time


running = True
# save the script path for the shutdown hook
script_path = path.dirname(path.abspath(__file__))

# the reminders
reminders = []


def run_loop():
  global running, reminders
  
  while running:
    # every second check for reminders
    time.sleep(1)

    # the current time in seconds
    curr_time = time.time()
    
    # open the file for reading and writing
    with open(reminders_file, "r+") as file_:
      # parse the json data
      try:
        reminders = json.load(file_)["reminders"]
      except (json.JSONDecodeError, KeyError):
        # force re-write
        write_default(True)
        continue

      for reminder in reminders:
        message = reminder["text"]
        # make sure the time has come/passed
        if int(reminder["time"]) > curr_time:
          continue
        # send the alert
        alert(message)

        # remove this reminder
        reminders.remove(reminder)

      # write new list
      write_reminders(file_, reminders)


def shutdown_hook():
  # let the user know
  alert("Alert background process shutdown unexpectedly")
  # try to run itself again
  try:
    os.execv(script_path, [ "python3.8" ] + sys.argv)
  except PermissionError:
    alert("Failed to restart background process")


def alert(message: str, title: str = title_text, /):
  subprocess.call([ "notify-send", title, message ])


if __name__ == "__main__":
  try:
    # register an exit handler
    atexit.register(shutdown_hook)

    # write the default file and begin the loop
    write_default()
    run_loop()
  except KeyboardInterrupt:
    print("Exiting due to escape key...")
    exit(-1)
